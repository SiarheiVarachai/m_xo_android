package org.hexlet.java101.gamexo.dialogs;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import org.hexlet.java101.gamexo.R;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class DialInfo extends DialogFragment implements OnClickListener {

    final String LOG_TAG = "myLogs";

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder adb = new AlertDialog.Builder(getActivity())
                .setTitle("Info!").setPositiveButton(R.string.ok, this)
                .setMessage(R.string.message_text);
        adb.setIcon(android.R.drawable.ic_dialog_info);

        return adb.create();
    }

    public void onClick(DialogInterface dialog, int which) {
        int i = 0;
        switch (which) {
            case Dialog.BUTTON_POSITIVE:
                i = R.string.ok;
                break;

        }
        if (i > 0)
            Log.d(LOG_TAG, "DialInfo: " + getResources().getString(i));
    }

    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        Log.d(LOG_TAG, "DialInfo: onDismiss");
    }

    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        Log.d(LOG_TAG, "DialInfo: onCancel");
    }
}
