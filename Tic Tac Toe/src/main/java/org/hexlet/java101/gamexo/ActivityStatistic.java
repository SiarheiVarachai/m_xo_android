package org.hexlet.java101.gamexo;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;

import org.hexlet.java101.gamexo.anim.OnTouchButton;

public class ActivityStatistic extends Activity implements View.OnClickListener{

    int allGame;
    int lossGame;
    int winGame;
    float ratio;

    TextView tv_Stat;

    Button btn_StatisticHome;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_statistic);


        allGame = 17;
        winGame = 15;
        lossGame = allGame - winGame;
        ratio = ((float)winGame) / ((float)lossGame);

        TextView tvStatAllGame = (TextView)findViewById(R.id.tvStatAllGame);
        TextView tvStatWinGame = (TextView)findViewById(R.id.tvStatWin);
        TextView tvStatLossGame = (TextView)findViewById(R.id.tvStatLoss);
        TextView tvStatRatio = (TextView)findViewById(R.id.tvStatRatio);

        Animation animStatisticText = AnimationUtils.loadAnimation(this, R.anim.statistic_text);

        tvStatAllGame.startAnimation(animStatisticText);
        tvStatWinGame.startAnimation(animStatisticText);
        tvStatLossGame.startAnimation(animStatisticText);
        tvStatRatio.startAnimation(animStatisticText);



        tvStatAllGame.setText("Games " + allGame);
        tvStatWinGame.setText("Win " + winGame);
        tvStatLossGame.setText("Loss " + lossGame);
        tvStatRatio.setText("Ratio " + ratio);

        Button btn_StatisticHome;


        btn_StatisticHome = (Button) findViewById(R.id.btn_StatisticHome);
        btn_StatisticHome.setOnClickListener(this);

        btn_StatisticHome.setOnTouchListener(new OnTouchButton());
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_StatisticHome:
                Intent intent = new Intent(this, ActivityHome.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_right_in, R.anim.slide_right_out);
                break;
            default:
                break;
        }

    }
}