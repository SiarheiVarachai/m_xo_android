package org.hexlet.java101.gamexo;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.app.Activity;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


public class ActivityMain extends Activity{

//    that welcomeDisplayDelay can change
//    by public void setWelcomeDisplayDelay(int i)
//    but not < that 3000 - animation takes that time
    private int welcomeDisplayDelay = 6000;

    ImageView imgLogo;
    LinearLayout llMainLogo, llMainText;
    TextView tvSplashScreenTextInfo, tvSplashScreenTextWait;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.main);

        imgLogo = (ImageView) findViewById(R.drawable.ic_launcher);
        tvSplashScreenTextInfo = (TextView) findViewById(R.id.tv_SplashScreenTextInfo);
        tvSplashScreenTextWait = (TextView) findViewById(R.id.tv_SplashScreenTextWait);
        llMainLogo = (LinearLayout) findViewById(R.id.llMainLogo);
        llMainText = (LinearLayout) findViewById(R.id.llMainText);

        Animation animation;
        Animation animationSlideLogoMain;

        animation = AnimationUtils.loadAnimation(this, R.anim.myalpha);
        animationSlideLogoMain = AnimationUtils.loadAnimation(this, R.anim.slide_logo_main);

        if (animationSlideLogoMain != null) {
            llMainLogo.startAnimation(animationSlideLogoMain);

        }

        if (animation != null) {
            tvSplashScreenTextWait.startAnimation(animation);
        }

        Animation animationSlideTextMain;
        animationSlideTextMain = AnimationUtils.loadAnimation(ActivityMain.this, R.anim.slide_text_main);
        if (animationSlideTextMain != null) {
            llMainText.startAnimation(animationSlideTextMain);
        }

        Thread welcomeThread = new Thread() {

            int wait = 0;

            @Override
            public void run() {
                try {
                    super.run();
                    while (wait < welcomeDisplayDelay) {
                        sleep(100);
                        wait += 100;
                    }
                } catch (Exception e) {
                } finally {
                    //finish();
                    startActivity(new Intent(ActivityMain.this, ActivityHome.class));
                    finish();
                }
            }
        };

        welcomeThread.start();
    }

    // set welcomeDisplayDelay new time in activity_preferences
    public void setWelcomeDisplayDelay(int i) {
        welcomeDisplayDelay = i;
    }

}
