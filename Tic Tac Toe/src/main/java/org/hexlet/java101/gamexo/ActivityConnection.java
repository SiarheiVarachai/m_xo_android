package org.hexlet.java101.gamexo;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;

import org.hexlet.java101.gamexo.anim.OnTouchButton;

public class ActivityConnection extends Activity implements View.OnClickListener{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_connection);

        Button btn_ConnectionGame;
        Button btn_ConnectionHome;

        btn_ConnectionGame = (Button) findViewById(R.id.btn_ConnectionGame);
        btn_ConnectionHome = (Button) findViewById(R.id.btn_ConnectionHome);

        btn_ConnectionGame.setOnClickListener(this);
        btn_ConnectionHome.setOnClickListener(this);

        btn_ConnectionGame.setOnTouchListener(new OnTouchButton());
        btn_ConnectionHome.setOnTouchListener(new OnTouchButton());
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_ConnectionGame:
//                btnAnim(1);
//                Intent intent = new Intent(this, ActivityGame.class);
//                startActivity(intent);
//                overridePendingTransition(R.anim.slide_left_in, R.anim.slide_left_out);
                break;
            case R.id.btn_ConnectionHome:
                btnAnim(2);
                Intent intentBtnConnectionHome = new Intent(this, ActivityHome.class);
                startActivity(intentBtnConnectionHome);
                overridePendingTransition(R.anim.slide_right_in, R.anim.slide_right_out);
                break;
            default:
                break;
        }
    }

    //number 1 - btn_ConnectionGame, 2 - btn_ConnectionHome
    private void btnAnim(int number) {

        Button btn_ConnectionGame;
        Button btn_ConnectionHome;

        btn_ConnectionGame = (Button) findViewById(R.id.btn_ConnectionGame);
        btn_ConnectionHome = (Button) findViewById(R.id.btn_ConnectionHome);

        Animation animationDisappearBtn;
        animationDisappearBtn = AnimationUtils.loadAnimation(this, R.anim.home_btns_disappear);

        // проверяем. Все исчезают, кроме нажатой
        if (number != 1) {
            if (animationDisappearBtn != null) {
                btn_ConnectionGame.startAnimation(animationDisappearBtn);
            }
        }

        if (number != 2) {
            if (animationDisappearBtn != null) {
                btn_ConnectionHome.startAnimation(animationDisappearBtn);
            }
        }
    }


}
