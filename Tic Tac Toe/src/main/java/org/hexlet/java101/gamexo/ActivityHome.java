package org.hexlet.java101.gamexo;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;

import org.hexlet.java101.gamexo.anim.OnTouchButton;
import org.hexlet.java101.gamexo.preferences.Preferences;

public class ActivityHome extends Activity implements View.OnClickListener {

    //TODO Рефакторить код, по примеру кода из ветки game_field, ибо не читаемо

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_home);

        Button btn_HomeGameHuman;
        Button btn_HomeGameBot;
        Button btn_HomeGameRemote;
        Button btn_HomeStatistic;
        Button btn_HomePrefs;

        btn_HomeGameBot = (Button) findViewById(R.id.btn_HomeGameBot);
        btn_HomeGameHuman = (Button) findViewById(R.id.btn_HomeGameHuman);
        btn_HomeGameRemote = (Button) findViewById(R.id.btn_HomeGameRemote);
        btn_HomeStatistic = (Button) findViewById(R.id.btn_HomeStatistic);
        btn_HomePrefs = (Button) findViewById(R.id.btn_HomePrefs);

        Animation animationLogoHome;
        ImageView imgLogo;
        imgLogo = (ImageView) findViewById(R.id.img_HomePicture);

        animationLogoHome = AnimationUtils.loadAnimation(this, R.anim.home_logo_anim);

        if (animationLogoHome != null) {
            imgLogo.startAnimation(animationLogoHome);

        }

        btn_HomeGameHuman.setOnClickListener(this);
        btn_HomeGameBot.setOnClickListener(this);
        btn_HomeGameRemote.setOnClickListener(this);
        btn_HomeStatistic.setOnClickListener(this);
        btn_HomePrefs.setOnClickListener(this);

        btn_HomeGameHuman.setOnTouchListener(new OnTouchButton());
        btn_HomeGameBot.setOnTouchListener(new OnTouchButton());
        btn_HomeGameRemote.setOnTouchListener(new OnTouchButton());
        btn_HomeStatistic.setOnTouchListener(new OnTouchButton());
        btn_HomePrefs.setOnTouchListener(new OnTouchButton());

    }

    @Override
    public void onClick(View view) {

        //TODO Вызывать правильные активити, передавать нужные аргументы

        switch (view.getId()) {
//            case R.id.btn_HomeGameHuman:
//                btnAnim(1);
//                Intent intentBtnHomeGameHuman = new Intent(this, ActivityGame.class);
//                startActivity(intentBtnHomeGameHuman);
//                overridePendingTransition(R.anim.slide_left_in, R.anim.slide_left_out);
//                break;
//            case R.id.btn_HomeGameBot:
//               btnAnim(2);
//                Intent intentBtnHomeGameBot = new Intent(this, ActivityGame.class);
//                startActivity(intentBtnHomeGameBot);
//                overridePendingTransition(R.anim.slide_left_in, R.anim.slide_left_out);
//                break;
            case R.id.btn_HomeGameRemote:
                btnAnim(3);
                Intent intentBtnHomeGameRemote = new Intent(this, ActivityRemote.class);
                startActivity(intentBtnHomeGameRemote);
                overridePendingTransition(R.anim.slide_left_in, R.anim.slide_left_out);
                break;
            case R.id.btn_HomeStatistic:
                btnAnim(4);
                Intent intentBtnHomeStatistic = new Intent(this, ActivityStatistic.class);
                startActivity(intentBtnHomeStatistic);
                overridePendingTransition(R.anim.slide_left_in, R.anim.slide_left_out);
                break;
            case R.id.btn_HomePrefs:
                //TODO Подключить настройки, из них тянуть данные
                btnAnim(5);
                Intent intentBtnHomePref = new Intent(this, Preferences.class);
                startActivity(intentBtnHomePref);
                overridePendingTransition(R.anim.slide_left_in, R.anim.slide_left_out);
                break;
            default:
                break;
        }
    }


    //number 1 - HomeGameHuman, 2 - HomeGameBot, 3 - HomeGameRemote, 4 - HomeStatistic, 5 - HomePrefs
    private void btnAnim(int number) {

        //TODO Упростить анимацию; нажатая кнопка слишком долго "вдавливается"; сделать "флаг" для отключения анимации,
        //TODO .....ибо дебажить, и каждый раз ее видеть это боль

        Button btn_HomeGameBot;
        Button btn_HomeGameHuman;
        Button btn_HomeGameRemote;
        Button btn_HomeStatistic;
        Button btn_HomePrefs;

        btn_HomeGameHuman = (Button) findViewById(R.id.btn_HomeGameHuman);
        btn_HomeGameBot = (Button) findViewById(R.id.btn_HomeGameBot);
        btn_HomeGameRemote = (Button) findViewById(R.id.btn_HomeGameRemote);
        btn_HomeStatistic = (Button) findViewById(R.id.btn_HomeStatistic);
        btn_HomePrefs = (Button) findViewById(R.id.btn_HomePrefs);

        Animation animationDisappearBtn;
        animationDisappearBtn = AnimationUtils.loadAnimation(this, R.anim.home_btns_disappear);

    // проверяем. Все исчезают, кроме нажатой
        if (number != 1) {
            if (animationDisappearBtn != null) {
                btn_HomeGameHuman.startAnimation(animationDisappearBtn);
            }
        }

        if (number != 2) {
            if (animationDisappearBtn != null) {
                btn_HomeGameBot.startAnimation(animationDisappearBtn);
            }
        }

        if (number != 3) {
            if (animationDisappearBtn != null) {
                btn_HomeGameRemote.startAnimation(animationDisappearBtn);
            }
        }

        if (number != 4) {
            if (animationDisappearBtn != null) {
                btn_HomeStatistic.startAnimation(animationDisappearBtn);
            }
        }

        if (number != 5) {
            if (animationDisappearBtn != null) {
                btn_HomePrefs.startAnimation(animationDisappearBtn);
            }

        }
    }



}
