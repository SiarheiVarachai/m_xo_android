package org.hexlet.java101.gamexo.anim;

import android.annotation.TargetApi;
import android.os.Build;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;

public class OnTouchButton implements View.OnTouchListener {
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1)
    @Override
    public boolean onTouch(View view, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                view.animate().setDuration(1000).scaleX(.8f).scaleY(.8f).setInterpolator(new DecelerateInterpolator());
                break;
            case MotionEvent.ACTION_UP:
                view.animate().setDuration(1000).scaleX(1).scaleY(1).setInterpolator(new AccelerateInterpolator());
                break;
            default:
                break;
        }
        return false;
    }
}
