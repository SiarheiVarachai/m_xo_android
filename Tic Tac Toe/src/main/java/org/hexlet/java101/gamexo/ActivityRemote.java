package org.hexlet.java101.gamexo;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;

import org.hexlet.java101.gamexo.anim.OnTouchButton;
import org.hexlet.java101.gamexo.preferences.Preferences;

public class ActivityRemote extends Activity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_remote);

        Button btn_RemoteJoinGameInet;
        Button btn_RemoteCreateGameInet;
        Button btn_RemoteJoinGameBluetooth;
        Button btn_RemoteCreateGameBluetooth;
        Button btn_RemoteStatistic;
        Button btn_RemotePreferences;

        btn_RemoteJoinGameInet = (Button) findViewById(R.id.btn_RemoteJoinGameInet);
        btn_RemoteCreateGameInet = (Button) findViewById(R.id.btn_RemoteCreateGameInet);
        btn_RemoteJoinGameBluetooth = (Button) findViewById(R.id.btn_RemoteJoinGameBluetooth);
        btn_RemoteCreateGameBluetooth = (Button) findViewById(R.id.btn_RemoteCreateGameBluetooth);
        btn_RemoteStatistic = (Button) findViewById(R.id.btn_RemoteStatistic);
        btn_RemotePreferences = (Button) findViewById(R.id.btn_RemotePreferences);

        btn_RemoteJoinGameInet.setOnClickListener(this);
        btn_RemoteCreateGameInet.setOnClickListener(this);
        btn_RemoteJoinGameBluetooth.setOnClickListener(this);
        btn_RemoteCreateGameBluetooth.setOnClickListener(this);
        btn_RemoteStatistic.setOnClickListener(this);
        btn_RemotePreferences.setOnClickListener(this);

        btn_RemoteJoinGameInet.setOnTouchListener(new OnTouchButton());
        btn_RemoteCreateGameInet.setOnTouchListener(new OnTouchButton());
        btn_RemoteJoinGameBluetooth.setOnTouchListener(new OnTouchButton());
        btn_RemoteCreateGameBluetooth.setOnTouchListener(new OnTouchButton());
        btn_RemoteStatistic.setOnTouchListener(new OnTouchButton());
        btn_RemotePreferences.setOnTouchListener(new OnTouchButton());
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_RemoteJoinGameInet:
                btnAnim(1);
                Intent intentBtnJoinGameInet = new Intent(this, ActivityConnection.class);
                startActivity(intentBtnJoinGameInet);
                overridePendingTransition(R.anim.slide_left_in, R.anim.slide_left_out);
                break;
            case R.id.btn_RemoteCreateGameInet:
                btnAnim(2);
                Intent intentBtnCreateGameInet = new Intent(this, ActivityConnection.class);
                startActivity(intentBtnCreateGameInet);
                overridePendingTransition(R.anim.slide_left_in, R.anim.slide_left_out);
                break;
            case R.id.btn_RemoteJoinGameBluetooth:
                btnAnim(3);
                Intent intentBtnJoinGameBluetooth = new Intent(this, ActivityConnection.class);
                startActivity(intentBtnJoinGameBluetooth);
                overridePendingTransition(R.anim.slide_left_in, R.anim.slide_left_out);
                break;
            case R.id.btn_RemoteCreateGameBluetooth:
                btnAnim(4);
                Intent intentBtnCreateGameBluetooth= new Intent(this, ActivityConnection.class);
                startActivity(intentBtnCreateGameBluetooth);
                overridePendingTransition(R.anim.slide_left_in, R.anim.slide_left_out);
                break;
            case R.id.btn_RemoteStatistic:
                btnAnim(5);
                Intent intentBtnRemoteStatistic = new Intent(this, ActivityStatistic.class);
                startActivity(intentBtnRemoteStatistic);
                overridePendingTransition(R.anim.slide_left_in, R.anim.slide_left_out);
                break;
            case R.id.btn_RemotePreferences:
                btnAnim(6);
                Intent intentBtnRemotePreferences = new Intent(this, Preferences.class);
                startActivity(intentBtnRemotePreferences);
                overridePendingTransition(R.anim.slide_left_in, R.anim.slide_left_out);
                break;
            default:
                break;
        }
    }

    //number 1 - btn_RemoteJoinGameInet, 2 - btn_RemoteCreateGameInet, 3 - btn_RemoteJoinGameBluetooth, 4 - btn_RemoteCreateGameBluetooth
    //5 - btn_RemoteStatistic, 6 - btn_RemotePreferences
    private void btnAnim(int number) {

        Button btn_RemoteJoinGameInet;
        Button btn_RemoteCreateGameInet;
        Button btn_RemoteJoinGameBluetooth;
        Button btn_RemoteCreateGameBluetooth;
        Button btn_RemoteStatistic;
        Button btn_RemotePreferences;

        btn_RemoteJoinGameInet = (Button) findViewById(R.id.btn_RemoteJoinGameInet);
        btn_RemoteCreateGameInet = (Button) findViewById(R.id.btn_RemoteCreateGameInet);
        btn_RemoteJoinGameBluetooth = (Button) findViewById(R.id.btn_RemoteJoinGameBluetooth);
        btn_RemoteCreateGameBluetooth = (Button) findViewById(R.id.btn_RemoteCreateGameBluetooth);
        btn_RemoteStatistic = (Button) findViewById(R.id.btn_RemoteStatistic);
        btn_RemotePreferences = (Button) findViewById(R.id.btn_RemotePreferences);

        Animation animationDisappearBtn;
        animationDisappearBtn = AnimationUtils.loadAnimation(this, R.anim.home_btns_disappear);

        // проверяем. Все исчезают, кроме нажатой
        if (number != 1) {
            if (animationDisappearBtn != null) {
                btn_RemoteJoinGameInet.startAnimation(animationDisappearBtn);
            }
        }

        if (number != 2) {
            if (animationDisappearBtn != null) {
                btn_RemoteCreateGameInet.startAnimation(animationDisappearBtn);
            }
        }

        if (number != 3) {
            if (animationDisappearBtn != null) {
                btn_RemoteJoinGameBluetooth.startAnimation(animationDisappearBtn);
            }
        }

        if (number != 4) {
            if (animationDisappearBtn != null) {
                btn_RemoteCreateGameBluetooth.startAnimation(animationDisappearBtn);
            }
        }

        if (number != 5) {
            if (animationDisappearBtn != null) {
                btn_RemoteStatistic.startAnimation(animationDisappearBtn);
            }
        }

        if (number != 6) {
            if (animationDisappearBtn != null) {
                btn_RemotePreferences.startAnimation(animationDisappearBtn);
            }
        }

    }
}
