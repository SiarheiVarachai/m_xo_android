package org.hexlet.java101.gamexo.preferences;

import android.annotation.TargetApi;
import android.app.DialogFragment;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.view.View;
import android.widget.Button;

import org.hexlet.java101.gamexo.R;
import org.hexlet.java101.gamexo.dialogs.DialInfo;

public class Preferences extends PreferenceActivity implements View.OnClickListener {

    /** TODO
     * После ввода имени, заменить текст "Please enter your name" на имя
     * FieldSize пока убрать, "закоментить" ибо у нас 3х3 пока что
     * Set Number Checked Signs туда же
     * Set amount games to win, пусть будет ввод цыфр, а вдруг хочет игрок 99 игр для победы
     * Знак (Х\О) игрока все таки сделать toggle_button
     * layout привести в соответсвие с основной темой (home_layout), ну пока это не важно, дизайн потом
     */

    private Button btnBack;
    private Button btnInfo;
    DialogFragment dlg;

    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.pref);
        setContentView(R.layout.activity_preferences);

        btnBack = (Button)findViewById(R.id.btnBack);
        btnBack.setOnClickListener(this);

        btnInfo = (Button)findViewById(R.id.btnInfo);
        btnInfo.setOnClickListener(this);

        dlg = new DialInfo();
    }
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case (R.id.btnBack):
            {
                Preferences.this.finish();
                break;
            }
            case (R.id.btnInfo):
            {
                dlg.show(getFragmentManager(), "dlg");
            }
        }
    }
}

